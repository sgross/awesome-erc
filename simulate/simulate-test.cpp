#include <vector>
#include "simulate.h"
#include "inc/catch.hpp"

TEST_CASE("power will be just added together", "[total_power]") {
	std::vector<double> power_data;
	REQUIRE(aerc::total_power(power_data) == 0);
	const int n = 13;
	for(int i = 0; i < n; i++) {
		power_data.push_back(1);
	}
	REQUIRE(aerc::total_power(power_data) == n);
}
