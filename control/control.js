#!/usr/bin/env/ node
'use strict';

const http = require('http');
const args = process.argv
const orion_hostname = (
	(args.length == 2) || ((args.length == 3) && args[1].endsWith('.js')))?
	args[args.length - 1]: 'orion';
const delay_ms = 5000;
const updatesCount = 1  // how many times to do updateContext
var updateContextOptions = {
	hostname: orion_hostname,
	port: 1026,
	path: '/v1/updateContext',
	method: 'POST',
	headers: {
		'Content-Type': 'application/json',
		'Accept': 'application/json'
	}
}

const houseUpdatePayload = function (name, residents_count, pv_area) {
	return JSON.stringify({
	"contextElements": [
	{
		"type": "House",
		"isPattern": "false",
		"id": name,
		"attributes": [
		{
			"name": "residents_count",
			"type": "integer",
			"value": residents_count
		},
		{
			"name": "pv_area",
			"type": "float",
			"value": pv_area
		}
		]
	}
	],
	"updateAction": "APPEND"
	});
}

const result_check = function (resp) {
	resp.on('data', function (chunk) {
		console.log('Response: ' + chunk);
	});
}

var i = 0;
global['updateContextLoop'] = setInterval(function () {
	var name = 'House' + i;
	var residents_count = i;
	var pv_area = 100 + 10 * i;
	i += 1;
	var payload = houseUpdatePayload(name, residents_count, pv_area);
	updateContextOptions.headers['Content-Length'] = payload.length;
	var req = http.request(updateContextOptions, result_check);
	req.write(payload);
	req.end();
	if (updatesCount <= i){
		clearInterval(global['updateContextLoop']);
	}
}, delay_ms);
